import React from 'react';
import { Jumbotron, Button, Row, Col , Nav ,NavLink} from 'react-bootstrap';
import {Link} from 'react-scroll'
import BgImage from './../images/bg-banner.jpg'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faYoutube,
  faFacebook,
  faTwitter,
  faInstagram
} from '@fortawesome/free-brands-svg-icons'


export default function Banner() {
  return(
    <React.Fragment>
  	<Row id="Banner" className="no-gutters " >
  		<Col>
  			<Jumbotron className="jumbotron-fluid" id="Jumbotron" >
          <div className="col-5" id="BannerContent">
    				<h1 className="text-white">FULL STACK DEVELOPER & SOFTWARE ENGINEER</h1>
    				<p className="text-white"><em>Specializing in custom theme development. If you're a business seeking a web presence or are looking to hire, contact me by clicking <NavLink as={Link} to="Contacts" smooth={true} className="clickHere">here</NavLink></em></p>
          </div>
          <div className="social-container">
          <a href="https://www.facebook.com/marcbids/" className="facebook social" target="_blank">
            <FontAwesomeIcon icon={faFacebook} size="3x"/>
          </a>
          <a href="https://www.youtube.com/channel/UC1QUzfH0BwWotW18wa8IZxw" className="youtube social" target="_blank">
            <FontAwesomeIcon icon={faYoutube} size="3x"/>
          </a>
          <a href="https://twitter.com/MarcoBowBow" className="twitter social" target="_blank">
            <FontAwesomeIcon icon={faTwitter} size="3x"/>
          </a>
          <a href="https://www.instagram.com/marcbids/" className="instagram social" target="_blank">
            <FontAwesomeIcon icon={faInstagram} size="3x"/>
          </a>
          </div>
  			</Jumbotron>
  		</Col>
  	</Row>
    </React.Fragment>
  );
}