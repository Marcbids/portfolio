import React from 'react'
import {Container, Carousel, Card , CardDeck} from 'react-bootstrap'
import responsive from './../images/carousel-images/responsive.jpg'
import mern from './../images/carousel-images/mern.jpg'
import nextjs from './../images/carousel-images/nextjs.jpg'
import responsiveIcon from './../images/carousel-images/responsive-icon.jpg'
import mernIcon from './../images/carousel-images/mern-icon.jpg'
import nextIcon from './../images/carousel-images/next-icon.jpg'
 
export default function MySkill () {

	function Info () {
		return(
			<CardDeck id="MySkillsInfo">
				<Card>
					<Card.Img variant="top" src={responsiveIcon} />
					<Card.Body>
						<Card.Title><u>Responsive Website</u></Card.Title>
						<Card.Text>
						Responsive web design can improve user experience which will translate into a positive perception of your brand and business.
						</Card.Text>
					</Card.Body>
				</Card>
				<Card>
					<Card.Img variant="top" src={mernIcon} />
					<Card.Body>
						<Card.Title><u>MERN Stack</u></Card.Title>
						<Card.Text>
						It creates web applications faster, as it uses virtual DOM which compares the components’ previous states and updates only the items in the Real DOM that were changed, instead of updating all of the components again.
						</Card.Text>
					</Card.Body>
				</Card>
				<Card>
					<Card.Img variant="top" src={nextIcon} />
					<Card.Body>
						<Card.Title><u>NEXT JS</u></Card.Title>
						<Card.Text>
						Next. js is clever enough to only load the Javascript and CSS that are needed for any given page. This makes for much faster page loading times, as a user's browser doesn't have to download Javascript and CSS that it doesn't need for the specific page the user is viewing.
						</Card.Text>
					</Card.Body>
				</Card>
			</CardDeck>
		)
	}
	return(
		<React.Fragment>
			<div id="MySkills">
				<Container>
					<h2 align="center">My Skills</h2>
					<div id="MySkillsContent">
						<Carousel id="MySkillsCarousel">

							<Carousel.Item interval={3000} width="100%">
								<img
								className="d-block w-100"
								src={responsive}
								alt="First slide"
								/>
								<Carousel.Caption>
									<div className="skillsBG">
										<h3>Responsive Website</h3>
									</div>
								</Carousel.Caption>
							</Carousel.Item>


							<Carousel.Item interval={3000}>
								<img
								className="d-block w-100"
								src={mern}
								alt="Third slide"
								/>
								<Carousel.Caption>
									<div className="skillsBG">
										<h3>MERN Stack</h3>
									</div>
								</Carousel.Caption>
							</Carousel.Item>


							<Carousel.Item interval={3000}>
								<img
								className="d-block w-100"
								src={nextjs}
								alt="Third slide"
								/>
								<Carousel.Caption>
									<div className="skillsBG">
										<h3>Next JS</h3>
									</div>
								</Carousel.Caption>
							</Carousel.Item>

						</Carousel>
					</div>
					<Info />
				</Container>
			</div>
		</React.Fragment>
	)
}