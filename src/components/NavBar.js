import React, {useState} from 'react'
import { Navbar, Nav, Image, Row, Col ,Bars, NavLink } from 'react-bootstrap'
import {Link} from 'react-scroll'
import Logo from './../images/logo.png'


export default function NavBar () {
	const [navbar, setNavbar] = useState(false)

	const changeBackground = () => {
		if(window.scrollY >= 800) {
			setNavbar(true)
		} else {
			setNavbar(false)
		}
	}
	

	window.addEventListener('scroll', changeBackground)

	return (
		<React.Fragment>
		<Navbar id="Navbar" variant="dark" expand="lg" className={navbar? 'navbar active fixed-top' : 'navbar fixed-top'}>
			<Nav.Link to="/" id="navLogo">
			 <Image src={Logo} width="50px" rounded />
			</Nav.Link>
		 	<Navbar.Toggle aria-controls="basic-navbar-nav" />
	 			<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end ">
					<NavLink as={Link} to="AboutMe" smooth={true} className="text-white">Home</NavLink>
	 				<NavLink as={Link} to="MyWork"  smooth={true} className="text-white">My Work</NavLink>
	 				<NavLink as={Link} to="MySkills"  smooth={true} className="text-white">My Skills</NavLink>
	 				<NavLink as={Link} to="Contacts" smooth={true} className="text-white">Contact</NavLink>
			 	</Navbar.Collapse>
		</Navbar>
		</React.Fragment>
	)
}
