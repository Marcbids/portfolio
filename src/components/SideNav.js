import React from 'react'
import {Link} from 'react-scroll'

export default function SideNav() {
	return(
		<div id="mySidenav" className="sidenav">
	        <Link to="AboutMe" id="about" smooth={true}>About Me</Link>
	        <Link to="MySkills" id="myskills" smooth={true}>My Skills</Link>
	        <Link to="MyWork" id="mywork" smooth={true}>My Work</Link>
	        <Link to="Contacts" id="contact" smooth={true}>Contact</Link>
      	</div>
  	)
}