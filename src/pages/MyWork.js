import React from 'react'
import {Container, CardDeck, Card, Button} from 'react-bootstrap'
import firstWebsite from './../images/projects-images/first-website.png'
import bookingSystem from './../images/projects-images/booking-system.png'
import budgetTracking from './../images/projects-images/budget-tracking.png'

export default function MyWork() {
	return (
		<React.Fragment>
			<div id="MyWork" className="bg-dark">
				<Container >
				<h2 align="center" className="text-white">My Work</h2>
				<CardDeck id="cardDeck">
					<Card>
						<Card.Img variant="top" src={firstWebsite} />
						<Card.Body>
							<Card.Title>My First Wepage</Card.Title>
							<Card.Text>
							This website was created using HTML, CSS, Gitlab
							</Card.Text>
						</Card.Body>
						<Card.Footer>
							<Button href="https://marcbids.gitlab.io/capstone1/" target="_blank"> Visit </Button>
						</Card.Footer>
					</Card>

					<Card>
						<Card.Img variant="top" src={bookingSystem} />
						<Card.Body>
							<Card.Title>Booking System</Card.Title>
							<Card.Text>
							My first Dynamic website was created using HTML, CSS, JS, Express JS, Node Js. The Database used is MongoDB
							</Card.Text>
						</Card.Body>
						<Card.Footer>
							<Button href="https://marcbids.gitlab.io/capstone2/" target="_blank"> Visit </Button>
						</Card.Footer>
					</Card>

					<Card>
						<Card.Img variant="top" src={budgetTracking} />
						<Card.Body>
							<Card.Title>Budget Tracking</Card.Title>
							<Card.Text>
							Helps you to keep track of your expenses and Income
							</Card.Text>
						</Card.Body>
						<Card.Footer>
							<Button href="https://capstone3-two.vercel.app/" target="_blank"> Visit </Button>
						</Card.Footer>
					</Card>
				</CardDeck>
				</Container>
			</div>
		</React.Fragment>
	)
}