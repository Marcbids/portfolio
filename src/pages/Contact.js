import React, {useState,useEffect} from 'react'
import {InputGroup, FormControl, Container, Form, Row, Col, Button} from 'react-bootstrap'
import Iframe from 'react-iframe'
import { Helmet } from 'react-helmet'
import { useForm } from 'react-hook-form';
import AppHelper from '../app-helper';

import Swal from 'sweetalert2';

export default function Contact () {

	const [subject, setSubject] = useState("");
	const [loading, setLoading] = useState(false)
	const { register, handleSubmit, watch, errors } = useForm();
	const onSubmit = data => {
		if (data.fname !== "" && data.lname !=="" && data.email !=="" && data.number !=="" && data.subject !=="" && data.message !==""){
			setLoading(true)
			const options = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					fname: data.fname,
					lname: data.lname,
					email: data.email,
					number: data.number,
					subject: data.subject,
					message: data.message
				})
			}
			fetch(`https://portfolio-mabid-api.herokuapp.com/api/send/`, options)
			.then((response) => response.json())
			.then(data => {
				Swal.fire(
					'Great!',
					`Email Successfully Sent!`,
					'success'
				)
				.then(result => {
					if (result.isConfirmed) {
						setLoading(false)
						window.location.reload();
					}
				})
			})
		} else {
			Swal.fire({
				title: 'Complete the form to send the message!',
				icon: 'warning',
				showClass: {
					popup: 'animate__animated animate__fadeInDown'
				},
				hideClass: {
					popup: 'animate__animated animate__fadeOutUp'
				}
			})
		}
	}

	
	return (
		<React.Fragment>
		<div id="Contacts">
			<Container id="contact-container">
				<div id="contact-div">
					<h2 align="center">Contact</h2>
					<p>For inquiries, please send me an email using the form below</p>
				
					<Form id='contact-form' onSubmit={handleSubmit(onSubmit)}>

						{/*<Form.Group as={Col} controlId="formGridState">
							<span>Please select a topic for your inquiry</span>
							<Form.Control as="select" value={subject} onChange={e => setSubject(e.target.value)}>
								<option>{null}</option>
								<option>How much?</option>
								<option>What are your expertise?</option>
							</Form.Control>
						</Form.Group> */}

						<div id="form">
							<Row className="no-gutters">
							<Col>
								<Row>
									<Col>
										<Form.Group>
												<Form.Label>First name</Form.Label>
											<Form.Control type="text" name="fname" ref={register} placeholder="Enter first name" className="input" />
										</Form.Group>
									</Col>

									<Col>
										<Form.Group>
												<Form.Label>Last name</Form.Label>
											<Form.Control type="text" name="lname" ref={register} placeholder="Enter last name" className="input" />
										</Form.Group>
									</Col>
								</Row>

								<Form.Group>
										<Form.Label>Email</Form.Label>
									<Form.Control type="email" name="email" ref={register} placeholder="Enter email" className="input" />
								</Form.Group>

								<Form.Group>
										<Form.Label>Contact</Form.Label>
									<Form.Control type="number" name="number" ref={register} placeholder="Enter Contact number" className="input" />
								</Form.Group>

								<Form.Group>
										<Form.Label>Subject</Form.Label>
									<Form.Control type="text" name="subject" ref={register} placeholder="Enter Subject" className="input" />
								</Form.Group>

								<Form.Group>
										<Form.Label>Message</Form.Label>
									<Form.Control as="textarea" name="message" ref={register} rows={3} placeholder="Enter message" maxLength='1500' className="input" />
								</Form.Group>
							</Col>
							</Row>
							<div id="buttonDiv">
								<Button variant="primary" type="submit" id="buttonSubmit" disabled={loading} className="input" >Submit</Button>
							</div>
						</div>

					</Form>
				</div>
			</Container>
		</div>
		</React.Fragment>
	)
}