import React from 'react'
import {Container} from 'react-bootstrap'
import MyPicture from './../images/my-picture.jpg'
import {Image, ProgressBar, Row, Col, Label} from 'react-bootstrap'


export default function AboutMe() {
	const HTML = 80;
	const CSS = 80;
	const JS = 80;
	const GitLab = 70;
	const Heroku = 60;
	const MongoDb = 80;
	const ExpressJs = 60;
	const ReactJs = 70;
	const NextJs = 70;
	const NodeJs = 70;

	

	return(
		<React.Fragment>
		<div className="bg-dark">
			<Container id="AboutMe">
					<h2 className="text-white" align="center">About me </h2>
				<div className="clearfix" id="AboutMe-Div">
					<div id="image"><Image src={MyPicture} width="280px" rounded /></div>
					<div id="info">	
						<p className="text-white"> Hi, my name is Marco and I'm a Full Stack Web Developer! I graduated from Cavite State Univesity. I took up Bachelor of Science in Information Technology. The reason why I chose to be a programmer is because I really want to hack websites back when I was in highschool.</p>
						<p className="text-white"> Recently, I joined Zuitt- Coding bootcamp to gain more knowledge about the new technologies that are being used in programming, nowadays. In there I learned how to build a <i className="text-primary font-weight-bold">Static Websites</i> and <i className="text-success font-weight-bold">Dynamic Websites</i>. They helped us to become a Full Stack Web Developer and now I'm confident enough to say that I am.</p>
					</div>
				</div>
				<Row className="justify-content-md-center">
					<Col className="col-12">
					<h2 className="text-white">Platforms</h2>
					</Col>
					<Col className="col-lg-5">
						<label className="text-white">HTML : </label>
						<ProgressBar now={HTML} label={`${HTML}%`} />

						<label className="text-white">CSS : </label>
						<ProgressBar now={CSS} label={`${CSS}%`} />

						<label className="text-white">JS : </label>
						<ProgressBar now={JS} label={`${JS}%`} />

						<label className="text-white">GitLab : </label>
						<ProgressBar now={GitLab} label={`${GitLab}%`} />

						<label className="text-white">Heroku : </label>
						<ProgressBar now={Heroku} label={`${Heroku}%`} />
					</Col>
					<Col className="col-lg-5">
						<label className="text-white">MongoDB : </label>
						<ProgressBar now={MongoDb} label={`${MongoDb}%`} />

						<label className="text-white">Express JS : </label>
						<ProgressBar now={ExpressJs} label={`${ExpressJs}%`} />

						<label className="text-white">React JS : </label>
						<ProgressBar now={ReactJs} label={`${ReactJs}%`} />

						<label className="text-white">Next JS : </label>
						<ProgressBar now={NextJs} label={`${NextJs}%`} />

						<label className="text-white">Node JS : </label>
						<ProgressBar now={NodeJs} label={`${NodeJs}%`} />
					</Col>
				</Row>
			</Container>
		</div>
		</React.Fragment>
	)
}