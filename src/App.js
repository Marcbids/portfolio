import React from 'react'
import NavBar from './components/NavBar'
import Footer from './components/Footer'
import SideNav from './components/SideNav'
import AboutMe from './pages/AboutMe'
import MyWork from './pages/MyWork'
import MySkills from './pages/MySkills'
import Contact from './pages/Contact'
import Banner from './components/Banner'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <React.Fragment>
      
      <NavBar />
      <Banner />
      <AboutMe />
      <MySkills />
      <MyWork />
      <Contact />
      <Footer />
    </React.Fragment>
  );
}

export default App;
